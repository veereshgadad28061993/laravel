<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use App\User;

class UsersController extends Controller
{
     public function index()
     {
     	$users = [

		'0' => [
				'firstName'	=>'Veeresh',
				'lastName'	=> 'Gadad'
	    ],

	    '1' => [
	    		'firstName'	=> 'Anmol',
				'lastName'	=> 'Prakash'
	    ]

	];
		return view('admin.users.users',compact('users'));
     }

     public function create()
     {
       return view('admin.users.create'); 	
     }

     public function store(Request $request)
     {
     	User::create($request->all());
     	return 'Success';
     	return $request->all();
     }
}