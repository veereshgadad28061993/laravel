<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use View;

class PagesController extends Controller
{
    
public function index()
{
	if (View::exists('pages.index'))
 	return view('pages.index')->with('text','<b>Laravel</b>')
 	->with('name','<b>Veeresh</b>')
 	->with(['location'=>'Europe','city'=>'iris']);
   // return view('pages.index',['text'=>'<b>Larvel</b>']);
    else
    	return 'No View Found';
}

public function profile()
{
	return view('pages.profile');
}

}
